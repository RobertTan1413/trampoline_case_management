package legacy.core

import grails.rest.RestfulController
import grails.validation.ValidationException
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional

/**
 * Why CompileStatic or CompileDynamic
 * https://www.tothenew.com/blog/compiling-groovy-code-statically/
 */
@CompileStatic
class BasicInformationController extends RestfulController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    BasicInformationController() {
        super(BasicInformation)
    }
    BasicInformationService basicInformationService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<BasicInformation> basicInformationList = basicInformationService.list(params)
        respond basicInformationList, model: [basicInformationCount: basicInformationService.count()]
    }

    @CompileDynamic
    def search(String q, Integer max ) {
        if (q) {
            respond basicInformationService.findByNameLike("%${q}%".toString(), [max: Math.min( max ?: 10, 100)])
        }
        else {
            respond([])
        }
    }

}
