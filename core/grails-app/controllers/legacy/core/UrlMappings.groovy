package legacy.core

class UrlMappings {

    static mappings = {
        get"/api/guest/$controller(.$format)?"(action: "index")
        get"/api/guest/$controller/$id(.$format)?"(action: "find")
        post "/api/guest/$controller(.$format)?"(action:"save")
        put "/api/guest/$controller/$id(.$format)?"(action:"update")
        patch "/api/guest/$controller/$id(.$format)?"(action:"patch")
        delete "/api/guest/$controller/$id(.$format)?"(action:"delete")

        delete "/$controller/$id(.$format)?"(action:"delete")
        get "/$controller(.$format)?"(action:"index")
        get "/$controller/$id(.$format)?"(action:"show")
        post "/$controller(.$format)?"(action:"save")
        put "/$controller/$id(.$format)?"(action:"update")
        patch "/$controller/$id(.$format)?"(action:"patch")

        '/basicinformations'(resources: 'BasicInformation') {
            collection {
                '/search'(controller: 'BasicInformation', action: 'search')
            }
        }

        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
