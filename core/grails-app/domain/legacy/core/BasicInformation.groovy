package legacy.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class BasicInformation {
    String ref
    String name
    Date dateOfBirth = (new Date() - 1)
    String gender
    String race
    Integer officerInCharge
    Integer coveringOfficer
    String otherRemarks
    Integer consent = 0
//    String address
//    String postalCode
//    String houseType


    static constraints = {
        ref blank: false
        name blank: false
        dateOfBirth max: new Date()
        gender blank: false
        race blank: false
        officerInCharge range:0..1000000
        coveringOfficer range:0..1000000
        otherRemarks blank: false
        consent blank: false
//        address blank: false
//        postalCode blank: false
//        houseType blank: false
    }
}
