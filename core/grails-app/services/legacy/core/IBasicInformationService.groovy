package legacy.core

import grails.gorm.services.Service
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@Service(BasicInformation)
interface IBasicInformationService {

    BasicInformation get(Serializable id)

    List<BasicInformation> list(Map args)

    Long count()

    void delete(Serializable id)

    BasicInformation save(BasicInformation basicInformation)
}