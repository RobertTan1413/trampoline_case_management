package legacy.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.Transactional


@GrailsCompileStatic
@Transactional
abstract class BasicInformationService implements IBasicInformationService {

    @Transactional(readOnly = true)
    List<BasicInformation> findByNameLike(String s, LinkedHashMap<String, Integer> stringIntegerLinkedHashMap) {
        return (List<BasicInformation>) BasicInformation.findByNameLike(s , stringIntegerLinkedHashMap)
    }
}
