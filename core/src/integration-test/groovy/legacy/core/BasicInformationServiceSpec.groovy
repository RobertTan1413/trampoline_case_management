package legacy.core

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class BasicInformationServiceSpec extends Specification {

    BasicInformationService basicInformationService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new BasicInformation(...).save(flush: true, failOnError: true)
        //new BasicInformation(...).save(flush: true, failOnError: true)
        //BasicInformation basicInformation = new BasicInformation(...).save(flush: true, failOnError: true)
        //new BasicInformation(...).save(flush: true, failOnError: true)
        //new BasicInformation(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //basicInformation.id
    }

    void "test get"() {
        setupData()

        expect:
        basicInformationService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<BasicInformation> basicInformationList = basicInformationService.list(max: 2, offset: 2)

        then:
        basicInformationList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        basicInformationService.count() == 5
    }

    void "test delete"() {
        Long basicInformationId = setupData()

        expect:
        basicInformationService.count() == 5

        when:
        basicInformationService.delete(basicInformationId)
        sessionFactory.currentSession.flush()

        then:
        basicInformationService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        BasicInformation basicInformation = new BasicInformation()
        basicInformationService.save(basicInformation)

        then:
        basicInformation.id != null
    }
}
