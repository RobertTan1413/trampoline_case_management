package legacy.core

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import spock.lang.Unroll

/**
 * reference : https://guides.grails.org/grails3/grails-test-domain-class-constraints/guide/index.html
 */
class BasicInformationSpec extends Specification  implements DomainUnitTest<BasicInformation> {

    def setup() {
    }

    def cleanup() {
    }

    void "test ref cannot be blank"() {
        when:
        domain.ref = ''

        then:
        !domain.validate(['ref'])
        domain.errors['ref'].code == 'blank'
    }

    void "test name cannot be blank"() {
        when:
        domain.name = ''

        then:
        !domain.validate(['name'])
        domain.errors['name'].code == 'blank'
    }

    void "test dateOfBirth cannot exceed current date"() {
        when:
        domain.dateOfBirth = new Date() + 1

        then:
        !domain.validate(['dateOfBirth'])
        domain.errors['dateOfBirth'].code == 'max.exceeded'
    }

    @Unroll('BasicInformationSpec.validate() with officerInCharge: #value should have returned #expected with errorCode: #expectedErrorCode')
    void "test officerInCharge in range value"() {
        when:
        domain.officerInCharge = value

        then:
        expected == domain.validate(['officerInCharge'])
        domain.errors['officerInCharge']?.code == expectedErrorCode

        where:
        value                  | expected | expectedErrorCode
        0                      | true     | null
        0.5                    | true     | null
        1000000                | true     | null
        1000000.5              | true     | null
        -1                     | false    | 'range.toosmall'
        -1000000               | false    | 'range.toosmall'
        181000000              | false    | 'range.toobig'
    }

    void "test remarks cannot be blank"() {
        when:
        domain.otherRemarks = ''

        then:
        !domain.validate(['otherRemarks'])
        domain.errors['otherRemarks'].code == 'blank'
    }
}
