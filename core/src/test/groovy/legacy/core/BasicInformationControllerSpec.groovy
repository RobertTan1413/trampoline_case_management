package legacy.core

import spock.lang.*
import static org.springframework.http.HttpStatus.*
import grails.validation.ValidationException
import grails.test.hibernate.HibernateSpec
import grails.testing.web.controllers.ControllerUnitTest
import grails.testing.gorm.DomainUnitTest

/**
 * change from Specification to HibernateSpec
 */
class BasicInformationControllerSpec extends HibernateSpec implements ControllerUnitTest<BasicInformationController>, DomainUnitTest<BasicInformation> {

    static doWithSpring = {
        jsonSmartViewResolver(JsonViewResolver)
    }

    void populateValidParams(params) {
        assert params != null

        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
        //assert false, "TODO: Provide a populateValidParams() implementation for this generated test suite"
        params["ref"] = 'JSF100101'
        params["name"] = 'Valid Profile Name'
        params["gender"] = 'M'
        params["race"] = 'C'
        params["officerInCharge"] = '111'
        params["coveringOfficer"] = '112'
        params["otherRemarks"] = 'Additional Remarks #2'
    }

    void "Test the index action returns the correct response"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * list(_) >> [new BasicInformation(ref:"JSF10001", name: 'Apple', gender: "M", race: "C", officerInCharge: 111, coveringOfficer: 112, otherRemarks: 'test remark', dateOfBirth: "2020-01-12T00:00:00.000Z")]
            1 * count() >> 1
        }

        when:"The index action is executed"
            controller.index()

        then:"The response is correct"
            response.json.size() == 1
            response.json[0].name == 'Apple'
//            response.text == '[{ref: \"JSF10001\", name: \"Apple\", gender: \"M\", race: \"C\", officerInCharge: 111, coveringOfficer: 112, otherRemarks: \"test remark\", consent: 0, dateOfBirth: \"2020-01-12T00:00:00.00Z\"}]'
    }

    void "Test the save action with a null instance"() {
        when:
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'POST'
        controller.save(null)

        then:
        response.status == NOT_FOUND.value()
    }

    void "Test the save action correctly persists"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * save(_ as BasicInformation)
        }

        when:
        response.reset()
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'POST'
        populateValidParams(params)
        def basicInformation = new BasicInformation(params)
        basicInformation.id = 1

        controller.save(basicInformation)

        then:
        response.status == CREATED.value()
        response.json
    }

    void "Test the save action with an invalid instance"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * save(_ as BasicInformation) >> { BasicInformation basicInformation ->
                throw new ValidationException("Invalid instance", basicInformation.errors)
            }
        }

        when:
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'POST'
        def basicInformation = new BasicInformation()
        basicInformation.validate()
        controller.save(basicInformation)

        then:
        response.status == UNPROCESSABLE_ENTITY.value()
        response.json.errors
    }

    void "Test the show action with a null id"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * get(null) >> null
        }

        when:"The show action is executed with a null domain"
        controller.show(null)

        then:"A 404 error is returned"
        response.status == NOT_FOUND.value()
    }

    void "Test the show action with a valid id"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * get(2) >> new BasicInformation()
        }

        when:"A domain instance is passed to the show action"
        controller.show(2)

        then:"A model is populated containing the domain instance"
        response.status == OK.value()
        response.json
    }

    void "Test the update action with a null instance"() {
        when:
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(null)

        then:
        response.status == NOT_FOUND.value()
    }

    void "Test the update action correctly persists"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * save(_ as BasicInformation)
        }

        when:
        response.reset()
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'PUT'
        populateValidParams(params)
        def basicInformation = new BasicInformation(params)
        basicInformation.id = 1

        controller.update(basicInformation)

        then:
        response.status == OK.value()
        response.json
    }

    void "Test the update action with an invalid instance"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * save(_ as BasicInformation) >> { BasicInformation basicInformation ->
                throw new ValidationException("Invalid instance", basicInformation.errors)
            }
        }

        when:
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'PUT'
        def basicInformation = new BasicInformation()
        basicInformation.validate()
        controller.update(basicInformation)

        then:
        response.status == UNPROCESSABLE_ENTITY.value()
        response.json.errors
    }

    void "Test the delete action with a null instance"() {
        when:
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(null)

        then:
        response.status == NOT_FOUND.value()
    }

    void 'test the search action finds results'() {
        given:
        controller.basicInformationService = Stub(BasicInformationService) {
            findByNameLike(_, _) >> [new BasicInformation(ref:"JSF10001", name: 'Apple', officerInCharge: 111, coveringOfficer: 112, remarks: 'test remark')]
        }
        when: 'A query is executed that finds results'
        controller.search('pp', 10)

        then: 'The response is correct'
        response.json.size() == 1
        response.json[0].name == 'Apple'
    }

    void "Test the delete action with an instance"() {
        given:
        controller.basicInformationService = Mock(BasicInformationService) {
            1 * delete(2)
        }

        when:
        request.contentType = JSON_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(2)

        then:
        response.status == NO_CONTENT.value()
    }
}