1
grails create-app legacy.core.core --profile rest-api --features json-views,hibernate5

2
grailsw.bat create-domain-class BasicInformation
3
grailsw.bat generate-all BasicInformation
4 reference https://plugins.grails.org/plugin/grails/geb (remark: grailsw.bat install-dependency org.gebish:geb-core:3.2)

add dynamic finders:
http://gorm.grails.org/latest/hibernate/manual/index.html#finders

5 TODO
curl -i -H "Accept: application/json" localhost:8080/basicinformations/search


TODO:
to setup and test
curl -i -H "Accept: application/json" localhost:8080/BasicInformation/1.json
curl -i -H "Content-Type:application/json" -X GET http://localhost:8080/BasicInformation/1.json

curl -i -H "Content-Type:application/json" -X POST localhost:8080/BasicInformation -d '{"ref":"JSF100100","name":"Profile Name","officerInCharge":111,"coveringOfficer":112,"remarks":"additional remarks #1"}'


result:
HTTP/1.1 422
X-Application-Context: application:development
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Thu, 26 Dec 2019 10:11:57 GMT

{"total":5,"_embedded":{"errors":[{"message":"Property [coveringOfficer] of class [class legacy.core.BasicInformation] cannot be null","path":"/BasicInformation/index","_links":{"self":{"href":"http://localhost:8080/BasicInformation/index"}}},{"message":"Property [name] of class [class legacy.core.BasicInformation] cannot be null","path":"/BasicInformation/index","_links":{"self":{"href":"http://localhost:8080/BasicInformation/index"}}},{"message":"Property [officerInCharge] of class [class legacy.core.BasicInformation] cannot be null","path":"/BasicInformation/index","_links":{"self":{"href":"http://localhost:8080/BasicInformation/index"}}},{"message":"Property [ref] of class [class legacy.core.BasicInformation] cannot be null","path":"/BasicInformation/index","_links":{"self":{"href":"http://localhost:8080/BasicInformation/index"}}},{"message":"Property [dateOfBirth] of class [class legacy.core.BasicInformation] cannot be null","path":"/BasicInformation/index","_links":{"self":{"href":"http://localhost:8080/BasicInformation/index"}}}]}}


